<?php

use App\Http\Controllers\ArticleController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::middleware('auth')->group(function() {
    Route::get('/', [ArticleController::class, 'index']);
    Route::resource('articles', ArticleController::class);
    Route::resource('comments', CommentController::class)->only('store', 'destroy')->middleware('auth');
    Route::get('articles/publish/{article}', [ArticleController::class, 'article_grant'])->name('article.grant');
    Route::put('articles/permit/{article}', [ArticleController::class, 'article_permit'])->name('article.permit');
    Route::get('/users/{user_id}', [UsersController::class, 'profile'])->name('profiles');
    Route::put('/allow-comment/{comment}', [CommentController::class, 'allow_comment'])->name('allow-comment');
    Route::post('/articles/{article}/rate', [ArticleController::class, 'rate_article'])->name('rate.article');
});


Auth::routes();
