<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Satisfaction>
 */
class SatisfactionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'satisfied' => $this->faker->randomElement(array(true, false)),
            'user_id' => rand(1, 20),
            'article_id' => rand(1, 60)
        ];
    }
}
