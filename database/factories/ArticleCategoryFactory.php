<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ArticleCategory>
 */
class ArticleCategoryFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->randomElement([
                "Politics",
                "Technology",
                "Entertainment",
                "Sports",
                "Health",
                "Business",
                "Science",
                "World News",
                "Local News",
                "Environment",
                "Opinion",
                "Education"
            ])
        ];
    }
}
