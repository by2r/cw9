<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Article>
 */
class ArticleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'title' => $this->faker->text(50),
            'body' => $this->faker->text(5000),
            'article_category_id' => rand(1, 12),
            'tag_id' => rand(1, 10),
            'user_id' => rand(1, 20),
            'created_at' => $this->faker->date(format: "Y-m-d"),
            'published_at' => $this->faker->date(format: 'Y-m-d')
        ];
    }
}
