<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Article;
use App\Models\ArticleCategory;
use App\Models\Comment;
use App\Models\Quality;
use App\Models\Relevance;
use App\Models\Satisfaction;
use App\Models\Tag;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
         \App\Models\User::factory(19)->create();

         \App\Models\User::factory()->create([
             'name' => 'Test User',
             'email' => 'test@example.com',
             'is_admin' => true
         ]);

         Tag::factory()->count(10)->create();

        foreach (ArticleCategory::ARTICLE_CATEGORIES as $category){
            DB::table('article_categories')->insert([
                'name' => $category,
                'created_at' => now(),
                'updated_at' => now()]);
        }

        Article::factory()->count(60)->create();

        Quality::factory()->count(300)->create();
        Relevance::factory()->count(300)->create();
        Comment::factory()->count(90)->create();
        Satisfaction::factory()->count(120)->create();
    }
}
