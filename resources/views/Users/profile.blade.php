@extends('layouts.app')
@section('content')
    <div class="my-4 border border-1 p-4">
        <p>
            <b>Name: </b>{{$user->name}}
        </p>
        <p>
            <b>Email: </b> {{$user->email}}
        </p>
        <p>
            <b>Joined at: </b> {{$user->created_at}}
        </p>
        <p>
            <b>Rating: </b> {{display_user_rating($user)}}
        </p>
    </div>
    <table class="text-center">
        <thead>
            <tr>
                <td colspan="7">Related Articles</td>
            </tr>
            <tr>
                <td>Title</td>
                <td>Category</td>
                <td>Quality</td>
                <td>Relevance</td>
                <td>Author</td>
                <td>Date Published</td>
                <td>Actions</td>
            </tr>
        </thead>

        <tbody>
            @foreach($user_articles as $article)
                @include('Articles.index_view')
            @endforeach
        </tbody>
    </table>
@endsection
