@extends('layouts.app')

@section('content')
    @include('notifications.alerts')
    <div>
        <a href="{{route('articles.create')}}" class="btn btn-primary my-3 btn-lg">Add an article</a>
    </div>
    <table class="text-center">
        <thead>
            <tr>
                <td colspan="7">All Articles</td>
            </tr>
            <tr>
                <td >Title</td>
                <td>Category</td>
                <td>Quality</td>
                <td>Relevance</td>
                <td>Author</td>
                <td>Date Published</td>
                <td>Actions</td>
            </tr>
        </thead>
        <tbody>
            @foreach($articles as $article)
                @if($article->published_at != null)
                    @include('Articles.index_view')
                @endif
            @endforeach
        </tbody>
    </table>


    <div class="justify-content-md-center p-5">
        <div class="col-md-auto">
            {{ $articles->links() }}
        </div>
    </div>

@endsection
