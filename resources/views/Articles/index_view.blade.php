<tr>
    <td>
        <a href="{{route('articles.show', ['article' => $article])}}">
            <h3 class="card-title py-4">{{$article->title}}</h3>
        </a>
    </td>

    <td>
        <p>{{$article->article_category->name}}</p>
    </td>

    <td>
        {{display_quality($article)}}
    </td>

    <td>
        {{display_relevance($article)}}
    </td>

    <td>
        <a href="{{route('profiles', ['user_id'=>$article->user->id])}}">{{$article->user->name}}</a>
    </td>

    <td>
        {{$article->published_at}}
    </td>

    <td>
        @can('update-article', $article)
            <a href="{{route('articles.edit', ['article' => $article])}}" class="btn btn-outline-warning my-3">Edit</a>
        @endcan
        @can('delete-article', $article)
            <form action="{{route('articles.destroy', ['article' => $article])}}" method="post">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-outline-danger">Delete</button>
            </form>
        @endcan
    </td>
</tr>
