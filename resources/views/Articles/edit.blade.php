@extends('layouts.app')
@section('content')
    <div>
        @include('notifications.alerts')
        <form action="{{route('articles.update', ['article' => $article])}}" method="post">

            @csrf
            @method('PUT')

            <div class="mb-3">
                <label for="title" class="form-label">Title</label>
                <input type="text" class="form-control" id="title" name="title" value="{{old('title') ?: $article->title}}">
            </div>

            <div class="mb-3">
                <label for="body" class="form-label">Body</label>
                <input type="text" class="form-control" id="body" name="body" value="{{old('body') ?: $article->body}}">
            </div>

            @can('is-admin')
                <div class="mb-3">
                    <label for="published_at" class="form-label">Date published</label>
                    <input type="date" class="form-control" id="published_at" name="published_at" value="{{old('published_at') ?: $article->published_at}}">
                </div>
            @endcan

            <div class="mb-3">
                <label for="tag" class="form-label">Tag</label>
                <select class="form-select" name="tag_id" id="tag">
                    <option selected value="{{null}}">Choose a tag</option>
                    @foreach($tags as $tag)
                        <option value="{{$tag->id}}"
                                @if(old('tag_id')==$tag->id || $article->tag->id == $tag->id) selected @endif
                        >{{$tag->name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="mb-3">
                <label for="category" class="form-label">Article Category</label>
                <select class="form-select" name="article_category_id" id="category">
                    <option selected value="{{null}}">Choose an article category</option>
                    @foreach($article_categories as $category)
                        <option value="{{$category->id}}"
                                @if(old('article_category_id')==$category->id || $article->article_category->id == $category->id) selected @endif
                        >{{$category->name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="mb-3">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
@endsection
