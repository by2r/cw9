@extends('layouts.app')
@section('content')
    <form action="{{route('article.permit', ['article' => $article])}}" method="post">
        @include('notifications.alerts')
        @csrf
        @method('PUT')
        <div class="mb-3">
            <label for="published_at">Publish At</label>
            <input type="date" class="form-control" name="published_at" id="published_at">
        </div>
        <div class="mb-3">
            <input type="submit" class="btn btn-warning">
        </div>
    </form>
@endsection
