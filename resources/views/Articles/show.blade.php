@extends('layouts.app')
@section('content')
    <div class="my-4">
        <h1>{{$article->title}}<span class="border border-3 bg-warning d-inline-block mx-4">{{$article->tag->name}}</span></h1>
        <p><b>Author: </b><a href="{{route('profiles', ['user_id'=>$article->user->id])}}">{{$article->user->name}}</a></p>
        <p><b>Quality: </b>{{display_quality($article)}}</p>
        <p><b>Relevance: </b>{{display_relevance($article)}}</p>
        <p><b>Satisfied users number: </b>{{display_satisfaction($article)}}</p>
        <p><b>Created at: </b>{{$article->created_at}}</p>
        <p><b>Published at: </b>{{$article->published_at}}</p>
        @can('is-admin', $article)
            <a href="{{route('article.grant', ['article' => $article])}}" class="btn btn-warning">Publishing date</a>
        @endcan
        <p class="my-5">{{$article->body}}</p>
    </div>
    <div class="row comments my-5">
        <div class="col-6">
            <form action="{{route('rate.article', ['article' => $article])}}" method="post">
                @csrf
                @method('POST')
                <div class="row">
                    <div class="col-4">
                        <select name="quality" class="form-select">
                            <option value="{{null}}">Select a quality mark</option>
                            @for($i=-5; $i<=5; $i++)
                                <option value="{{$i}}">{{$i}}</option>
                            @endfor
                        </select>
                    </div>
                    <div class="col-4">
                        <select name="relevance" class="form-select">
                            <option value="{{null}}">Select a relevance mark</option>
                            @for($i=-5; $i<=5; $i++)
                                <option value="{{$i}}">{{$i}}</option>
                            @endfor
                        </select>
                    </div>
                    <div class="col-4">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" id="satisfied" value="1" name="satisfied">
                            <label class="form-check-label" for="satisfied">
                                Satisfied
                            </label>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-info my-4">Submit</button>
                </div>
            </form>
        </div>

        <div class="col-6">
            <div class="border border-1 p-4">
                @include('notifications.alerts')
                <form action="{{route('comments.store')}}" method="post">
                    @csrf
                    @method("POST")
                    <input type="hidden" value="{{$article->id}}" name="article_id">
                    <div class="mb-3">
                        <label for="body" class="form-label">Leave a comment</label>
                        <input type="text" class="form-control" id="body" name="body">
                    </div>
                    <div class="mb-3">
                        <button type="submit" class="btn btn-outline-info btn-lg">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="">
        @foreach($comments as $comment)
            @if($comment->is_allowed || is_admin())
                <div class="p-3 border border-1">
                    <p>{{$comment->user->name}} commented at {{$comment->created_at}}</p>
                    <p>{{$comment->body}}</p>
                    @can('delete-comment', $comment)
                        <form action="{{route('comments.destroy', ['comment' => $comment])}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-outline-danger">Delete</button>
                        </form>
                    @endcan
                    @if(!$comment->is_allowed)
                        @can('is-admin')
                            <form action="{{route('allow-comment', ['comment' => $comment])}}" method="post">
                                @csrf
                                @method('PUT')
                                <button type="submit" class="btn btn-warning">Allow</button>
                            </form>
                        @endcan
                    @endif
                </div>
            @endif
        @endforeach
    </div>
@endsection
