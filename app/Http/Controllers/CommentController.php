<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request['user_id'] = $request->user()->id;
        $validated = $request->validate([
            'body' => 'required',
            'user_id' => 'required',
            'article_id' => 'required'
        ]);
        $comment = new Comment($validated);
        $comment->save();
        return redirect()->back()->with('success', 'Your comment is under review.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Comment $comment)
    {
        $comment->delete();
        return redirect()->back()->with('success', 'Comment deleted successfully');
    }

    public function allow_comment(Comment $comment) {
        $comment->is_allowed = true;
        $comment->save();
        return redirect()->back()->with('success', 'Success');
    }
}
