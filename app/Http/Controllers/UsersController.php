<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function profile($user_id) {
        $user = User::find($user_id);
        $user_articles = Article::where('user_id', $user->id)->get();
        return view('Users.profile', compact('user', 'user_articles'));
    }
}
