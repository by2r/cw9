<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\ArticleCategory;
use App\Models\Comment;
use App\Models\Quality;
use App\Models\Relevance;
use App\Models\Satisfaction;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $articles = Article::paginate(6);
        return view('Articles.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $article_categories = ArticleCategory::all();
        $tags = Tag::all();
        return view('Articles.create', compact('article_categories', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request['user_id'] = Auth::user()->id;
        $validated = $request->validate([
            'title' => 'required',
            'body' => 'required',
            'user_id' => 'required',
            'article_category_id' => 'required',
            'tag_id' => 'required',
            'published_at' => 'nullable'
        ]);
        $article = new Article($validated);
        $article->save();
        return redirect()->route('articles.index')->with('success', 'Article created successfully');
    }

    /**
     * Display the specified resource.
     */
    public function show(Article $article)
    {
        $comments = Comment::all()->where('article_id', $article->id);
        return view('Articles.show', compact('article', 'comments'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Article $article)
    {
        $tags = Tag::all();
        $article_categories = ArticleCategory::all();
        return view('Articles.edit', compact('article', 'tags', 'article_categories'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Article $article)
    {
        $request['user_id'] = Auth::user()->id;
        $validated = $request->validate([
            'title' => 'required',
            'body' => 'required',
            'published_at' => 'nullable',
            'tag_id' => 'required',
            'article_category_id' => 'required',
            'user_id' => 'required'
        ]);

        $article->update($validated);

        return redirect()->route('articles.index')->with('success', 'Article updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Article $article)
    {
        $article->delete();
        return redirect()->back()->with('success', 'Article deleted successfully');
    }
    public function article_grant($article) {
        return view('Articles.grant', compact('article'));
    }

    public function article_permit(Request $request, Article $article) {
        $validated = $request->validate([
            'published_at' => 'required'
        ]);
        $article->published_at = $validated['published_at'];
        $article->save();
        return redirect()->back()->with('success', 'Published date is ' . $article->published_at);
    }

    public function rate_article(Request $request, Article $article) {
        $validated = $request->validate([
           'quality' => 'nullable',
            'relevance' => 'nullable',
            'satisfied' => 'nullable'
        ]);
        if ($validated["quality"] != null){
            $quality = new Quality([
                'quality' => $validated['quality'],
                'user_id' => $request->user()->id,
                'article_id' => $article->id]);
            $quality->save();
        }
        if ($validated['relevance'] != null) {
            $relevance = new Relevance([
                'relevance' => $validated['relevance'],
                'user_id' => $request->user()->id,
                'article_id' => $article->id]);
            $relevance->save();
        }
        if (isset($validated["satisfied"]) && $validated["satisfied"] != null){
            $satisfaction = new Satisfaction([
                'satisfied' => $validated['satisfied'],
                'user_id' => $request->user()->id,
                'article_id' => $article->id]);
            $satisfaction->save();
        }
        return redirect()->back()->with('Success');
    }
}
