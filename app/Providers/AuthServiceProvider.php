<?php

namespace App\Providers;

// use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        //
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        $this->registerPolicies();

        Gate::define('update-article', function ($user, $article) {
            return $user->id == $article->user_id;
        });
        Gate::define('delete-article', function ($user, $article) {
            return $user->id == $article->user_id;
        });

        Gate::define('is-admin', function ($user) {
            return $user->is_admin;
        });

        Gate::define('delete-comment', function ($user, $comment) {
            return $comment->user_id == $user->id;
        });


    }
}
