<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Relevance extends Model
{
    use HasFactory;
    protected $fillable = ['relevance', 'user_id', 'article_id'];
}
