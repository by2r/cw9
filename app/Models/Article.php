<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;
    protected $fillable = ['title', 'body', 'article_category_id', 'tag_id', 'user_id', 'published_at'];

    public function article_category() {
        return $this->belongsTo(ArticleCategory::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function tag() {
        return $this->belongsTo(Tag::class);
    }

    public function qualities() {
        return $this->hasMany(Quality::class);
    }

    public function relevances() {
        return $this->hasMany(Relevance::class);
    }
}
