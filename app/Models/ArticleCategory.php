<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ArticleCategory extends Model
{
    const ARTICLE_CATEGORIES = [
        "Politics",
        "Technology",
        "Entertainment",
        "Sports",
        "Health",
        "Business",
        "Science",
        "World News",
        "Local News",
        "Environment",
        "Opinion",
        "Education"
    ];

    use HasFactory;
    protected $fillable = ['name'];

    public function articles() {
        return $this->hasMany(Article::class);
    }
}
