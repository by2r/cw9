<?php
function display_quality($article) {
    $qualities = \App\Models\Quality::where('article_id', $article->id)->get('quality')->toArray();
    $sum = 0;
    foreach($qualities as $quality) {
        $sum += $quality['quality'];
    }
    if(count($qualities) !== 0) {
        return round($sum/count($qualities), 1);
    }
    return 0;
}


function display_relevance($article) {
    $relevances = \App\Models\Relevance::where('article_id', $article->id)->get('relevance')->toArray();

    $sum = 0;
    foreach($relevances as $relevance) {
        $sum += $relevance['relevance'];
    }
    if(count($relevances) !== 0) {
        return round($sum/count($relevances), 1);
    }
    return 0;
}
function display_satisfaction($article) {
    $satisfactions = \App\Models\Satisfaction::where('article_id', $article->id)->where('satisfied', 1)->get('id')->count();
    return $satisfactions;
}


function display_user_rating($user) {
    $articles_count = \App\Models\Article::where('user_id', $user->id)->get('id')->count();

    $quality_of_articles = \App\Models\Quality::where('user_id', $user->id)->get('quality')->toArray();
    $quality_sum = 0;
    foreach($quality_of_articles as $quality) {
        $quality_sum += $quality['quality'];
    }

    $relevance_of_articles = \App\Models\Relevance::where('user_id', $user->id)->get('relevance')->toArray();
    $relevance_sum = 0;
    foreach($relevance_of_articles as $relevance) {
        $relevance_sum += $relevance['relevance'];
    }

    return round(($articles_count + $quality_sum + $relevance_sum)/3, 1);
}

function is_admin() {
    return \Illuminate\Support\Facades\Auth::user()->is_admin;
}
